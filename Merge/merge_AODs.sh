#!bin/bash
########################################################################
#                          SETUP PART                                  #
########################################################################

############################################
# array of input directories -
#     in each directory are AOD file to merge together

arr_INPUT_DIRS=(
/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/AOD/140520_100kEvents/user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_03_100000Evt_140520_FTFP_BERT_G4-10-6_v2.AOD_ESD_EXT0
)
#/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/AOD/140520_100kEvents/user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_03_100000Evt_140520_FTFP_BERT_21-0-108-Ath_v2.AOD_ESD_EXT0

############################################
# path for output merged AOD files
path_OUPTPUT="/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/AOD/"

N_EVENTS="100k"
########################################################################
#                     MAIN PART OF THE SCRIPT                          #
########################################################################

############################################
# current directory
cwd=$(pwd)


############################################
# LOOP over all conteiners
for dir_input in ${arr_INPUT_DIRS[@]}; do
	
	WORK_DIR=${dir_input}"/WORK_MERGE"
	
	mkdir -p ${WORK_DIR}
	cd ${WORK_DIR}
	
	MERGED_AODs_NAME_TMP=${dir_input#*.}
	MERGED_AODs_DOTS_TMP=${dir_input##*.}
	MERGED_AODs_NAME_TMP2=${MERGED_AODs_NAME_TMP/.${MERGED_AODs_DOTS_TMP}/}

	MERGED_AODs_NAME=${MERGED_AODs_NAME_TMP2##*.}"__"${N_EVENTS}".AOD.root"
	echo ${MERGED_AODs_NAME}
	
	
	rm merged.AOD.root
	
	############################################
	# setup
	setupATLAS
	asetup 21.0.108,Athena,gcc62
	
	Merge_tf.py --inputAODFile ${dir_input}/*.AOD.root --outputAODFile ${WORK_DIR}/merged.AOD.root
	
	cp merged.AOD.root ${path_OUPTPUT}/${MERGED_AODs_NAME}
	
done

cd ${cwd}
