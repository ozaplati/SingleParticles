#!bin/bash
########################################################################
#                          SETUP PART                                  #
########################################################################

############################################
# rucio containers
arr_container_baseName=(
user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta22_23_100000Evt_140520_FTFP_BERT_21-0-108-Ath_v2.AOD_ESD
user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_03_100000Evt_140520_FTFP_BERT_21-0-108-Ath_v2.AOD_ESD
user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_03_100000Evt_140520_FTFP_BERT_G4-10-6_v2.AOD_ESD
user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_03_100000Evt_140520_FTFP_BERT_ATL_G4-10-6_v2.AOD_ESD
user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_03_100000Evt_140520_FTFP_BERT_ATL_21-0-108-Ath_v2.AOD_ESD
user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta22_23_100000Evt_140520_FTFP_BERT_ATL_21-0-108-Ath_v2.AOD_ESD
user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta22_23_100000Evt_140520_FTFP_BERT_ATL_G4-10-6_v2_100k.AOD_ESD
user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta22_23_100000Evt_140520_FTFP_BERT_21-0-108-Ath_v2_100k.AOD_ESD
)

############################################
# working directory
working_dir="/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/AOD/140520_100kEvents/"

########################################################################
#                     MAIN PART OF THE SCRIPT                          #
########################################################################

############################################
# current directory
cwd=$(pwd)

############################################
# create working directory
mkdir -p ${working_dir}

############################################
# move to working directory
cd ${working_dir}

############################################
# setup
setupATLAS
voms-proxy-init -voms atlas
lsetup rucio

############################################
# LOOP over all conteiners
for container in ${arr_container_baseName[@]}; do
    AODs=${container}"_EXT0"
    logs=${container}".log"
    
    #######################################
    # Download AODs
    echo "AODs: "${AODs}
    rucio -v download ${AODs}
    
    #######################################
    # Download logs
    echo "logs: "${logs}
    rucio -v download ${logs}
done

cd ${cwd}
