evgenConfig.description = "Single Pi- with flat eta-phi and log Pt in [1, 100] GeV"
evgenConfig.keywords = ["singleParticle", "pi-"]
 
include("ParticleGun/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = -211
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.LogSampler(1000.0, 100000.0), eta=[-5.0, 5.0])
