evgenConfig.description = "Single electron with flat eta-phi and log Pt in [1, 100] GeV"
evgenConfig.keywords = ["singleParticle", "electron"]
 
include("ParticleGun/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 11
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.LogSampler(1000.0, 100000.0), eta=[2.2, 2.3])
