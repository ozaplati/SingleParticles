evgenConfig.description = "Single Pi+ with Pt in 10 GeV and eta 0.2"
evgenConfig.keywords = ["singleParticle", "pi+"]
 
include("ParticleGun/ParticleGun_Common.py")
 
import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = 211
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=10000.0, eta=0.2)
