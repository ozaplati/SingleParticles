#! -*- python -*-
evgenConfig.description = "Single particle gun for E/p event generation"
evgenConfig.keywords = ["singleParticle",]
evgenConfig.generators = ["ParticleGun"]
#evgenConfig.contact = ["zach.marshall@cern.ch"]
evgenConfig.contact = ["ota.zaplatilek@cern.ch"]


import ParticleGun as PG
import ROOT
from ParticleGun.samplers import *
class PEtaSampler(PG.MomSampler):
    "Create a 4-momentum vector from pt, eta, m and phi distributions/samplers."

    def __init__(self, momentum, eta, pid=211, phi=[0, math.pi*2.]):
        self.momentum = momentum
        self.eta = eta
        pdg_table = ROOT.TDatabasePDG.Instance()
        mass = pdg_table.GetParticle(pid).Mass()
        self.mass = mass
        self.phi = phi

    @property
    def momentum(self):
        "Momentum sampler"
        return self._momentum
    @momentum.setter
    def momentum(self, x):
        self._momentum = mksampler(x)

    @property
    def eta(self):
        "Pseudorapidity sampler"
        return self._eta
    @eta.setter
    def eta(self, x):
        self._eta = mksampler(x)

    @property
    def mass(self):
        "Mass sampler"
        return self._m
    @mass.setter
    def mass(self, x):
        self._m = mksampler(x)

    @property
    def phi(self):
        "Azimuthal angle sampler"
        return self._phi
    @phi.setter
    def phi(self, x):
        self._phi = mksampler(x)

    def shoot(self):
        """
        eta = - ln(tan(theta/2)) / 2
        => theta = 2 atan( exp(-eta) )
        """
        eta = self.eta()
        theta = 2 * math.atan(math.exp(-eta));
        p = self.momentum()
        phi = self.phi()
        pt = p * math.sin(theta)
        px = pt * math.cos(phi)
        py = pt * math.sin(phi)
        pz = p * math.cos(theta)
        m = self.mass()
        e = math.sqrt( p**2 + m**2 )
        v4 = ROOT.TLorentzVector(px, py, pz, e)
        return v4

pg = PG.ParticleGun()
pg.sampler.pid = 211 #PID
pg.sampler.mom = PEtaSampler(momentum=PG.LogSampler(500,31000), eta=[-0.7,-0.2,0.2,0.7], pid=211)
genSeq += pg

