####################################################################################
# Description
evgenConfig.description = "Single Pi+ with flat eta-phi and log Pt in [1, 50] GeV"

####################################################################################
# KeyWords
evgenConfig.keywords = ["singleParticle", "pi+"]
 
include("ParticleGun/ParticleGun_Common.py")

 
import ParticleGun as PG
####################################################################################
# setup single particle identification code - pid
genSeq.ParticleGun.sampler.pid = 211

####################################################################################
# setup single partilce momentum - using pt and eta intervals
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=PG.LogSampler(1000.0, 50000.0), eta=[2.2, 2.3])
