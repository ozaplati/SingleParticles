evgenConfig.description = "Single particle sample for testing the detector"
evgenConfig.keywords = ["singleParticle", "muon"]

include("ParticleGun/ParticleGun_Common.py")

import ParticleGun as PG
genSeq.ParticleGun.sampler.pid = PG.CyclicSeqSampler([-13, 13])
genSeq.ParticleGun.sampler.mom = PG.PtEtaMPhiSampler(pt=50000, eta=[-4,4])
