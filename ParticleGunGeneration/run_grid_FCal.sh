#!/bin/bash	
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
# name of python configuration file
#     - all configuration files are stored in Config directory
#     - use here only the name of configuration file
#     - value of runNumber have to be a part od CONFIG variable
#       e.q. 
#           CONFIG="MC15.999999.ParticleGun_single_piplus_eta_02_03.py"
#CONFIG="ParticleGun_single_piplus_eta_38_39_FCal.py"
CONFIG="PG_piplus_eta_38_39_lowP_FCal.py"


########################################################################
# run number 
#     - run number have to be the same here as well as for configuration 
#       file name above
#       e.q.
#           CONFIG="MC15.999999.ParticleGun_single_piplus_eta_02_03.py"
#           RUN_NUMBER="999999"
RUN_NUMBER="999999"

########################################################################
# number of events per one job
#     - use int not string
N_EVENTS_PER_JOB=1000

########################################################################
# number of jobs
#     - use int not string
N_JOBS=5000

########################################################################
# total number of events = N_JOBS * N_EVENTS_PER_JOB
#     - do not change
N_EVENTS_TOT="$(( ${N_JOBS}*${N_EVENTS_PER_JOB} ))"

########################################################################
# description
#     - for a name of output container
#     - for a Woking subDir name
kEVENTS=$((N_EVENTS_TOT/1000))
GRID_DESC="PG_piplus_eta_38_39_lowP_FCal_${kEVENTS}kEvt_011521"

echo ${GRID_DESC}
read -p "Press enter to continue"

########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################

########################################################################
# paths inicialization
BASE_DIR=`pwd`
echo "BASE_DIR: " $BASE_DIR
read -p "Press enter to continue"

CONFIG_DIR=${BASE_DIR}"/Configs"
WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${GRID_DESC}
OUTPUT_DIR=${BASE_DIR}"/Output_ENVT"

########################################################################
# make Work directories
mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}


########################################################################
# clear Working directory before submit
echo -e "\n\n"
echo "working directory have to be clean before grid submition"
echo "do you want to really remove all files/directories in:"
echo ${WORK_DIR_I}"/"
echo "[Y/N]"


while read CLEAR; do
   if [[ "${CLEAR}" == "Y" ]]; then
        echo -e "\n\n"
        ################################################################
        # Clear working directory
        rm -r ${WORK_DIR_I}/*
        
        
        ################################################################
        # test if jobOption config file exist
        if [ ! -f ${CONFIG_DIR}/${CONFIG} ]; then
            echo -e "\n\n"
            echo "ERROR"
            echo "configuration file:"
            echo "${CONFIG_DIR}/${CONFIG}"
            echo "does not exist!"
            break
        fi
        
        ################################################################
        # Copy jobOption config file to woking dirrectory
        cp ${CONFIG_DIR}/${CONFIG} ${WORK_DIR_I}/.
        
        ################################################################
        # go to working directory
        cd ${WORK_DIR_I}
        
        ################################################################
        # setup
        setupATLAS
        asetup  21.6.12,AthGeneration,here
        
        voms-proxy-init -voms atlas
        lsetup panda
        
        ################################################################
        # RUN with pathena
        echo -e "\n\n"
        echo "RUN with pathena"
        echo -e "\n\n"
        pathena --trf=\
"Generate_tf.py \
--ecmEnergy=13000 \
--runNumber=${RUN_NUMBER} \
--firstEvent=1 \
--maxEvents=${N_EVENTS_PER_JOB} \
--randomSeed=%RNDM:100 \
--jobConfig=${CONFIG} \
--outputEVNTFile=%OUT.EVNT.pool.root \
--steering=afterburn" \
--outDS user.ozaplati.singleParticleSample13TeV.MC15.${RUN_NUMBER}.${GRID_DESC} \
--nEventsPerJob=${N_EVENTS_PER_JOB} \
--nJobs=${N_JOBS}
        
        cd ${BASE_DIR}
        break
    elif [[ "${CLEAR}" == "N" ]]; then
        echo "end of program"
        cd ${BASE_DIR}
        break
    else
        echo "wrong value!"
        echo "use: \"Y\" as YES"
        echo "     \"N\" as NO"
    fi
done
