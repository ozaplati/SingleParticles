#!/bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
# name of python jobOption configuration file
#     - all configuration files are stored in Config directory
#     - use here only the name of configuration file
#     - value of runNumber have to be a part od CONFIG variable
#       e.q. 
#           CONFIG="MC15.999999.ParticleGun_single_piplus_eta_02_03.py"
CONFIG="MC15.999999.ParticleGun_single_piplus_eta_22_23.py"

########################################################################
# run number 
#     - run number have to be the same here as well as for configuration 
#       file name above
#       e.q.
#           CONFIG="MC15.999999.ParticleGun_single_piplus_eta_02_03.py"
#           RUN_NUMBER="999999"
RUN_NUMBER="999999"


########################################################################
# output EVNT file
#       - name of output root file
#         e.q.
#              OUTPUT_EVNT="single_piplus_eta_22_23_2kEvt.root"
OUTPUT_EVNT="test_ParticleGun_EoverP_Config_logP.root"

########################################################################
# number of events
N_EVENTS="10"

########################################################################
# description
#     - for a Woking subDir name
DESC="local_"${OUTPUT_EVNT/.root/}






########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################

########################################################################
# paths inicialization

BASE_DIR=`pwd`
CONFIG_DIR=${BASE_DIR}"/Configs"
WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${DESC}
OUTPUT_DIR=${BASE_DIR}"/Output_ENVT"


########################################################################
# make Work directories
mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}


if [ ! -f ${CONFIG_DIR}/${CONFIG} ]; then
    ####################################################################
    # The jobOption: ${CONFIG_DIR}/${CONFIG} does not exist
    # ->exit 
    echo -e "\n\n"
    echo "ERROR"
    echo "configuration file:"
    echo "${CONFIG_DIR}/${CONFIG}"
    echo "does not exist!"

else
    ####################################################################
    # OK, jobOption exits
    # lets prepare working directory
    
    ####################################################################
    # Copy jobOption config file to woking dirrectory
    cp ${CONFIG_DIR}/${CONFIG} ${WORK_DIR_I}/.
    
    ####################################################################
    # Setup
    cd ${WORK_DIR_I}
    
    setupATLAS
    asetup  21.6.12,AthGeneration,here
    
    ####################################################################
    # DEBUG print
    echo -e "\n\n"
    echo "RUN Generate_tf.py"
    echo -e "\n\n"
    
    ####################################################################
    # RUN Generate_tf.py
    Generate_tf.py \
--ecmEnergy 13000 \
--runNumber ${RUN_NUMBER} \
--firstEvent 1 \
--maxEvents ${N_EVENTS} \
--randomSeed 1234 \
--jobConfig ${CONFIG} \
--outputEVNTFile ${OUTPUT_DIR}/${OUTPUT_EVNT}
    
    cd ${BASE_DIR}
fi
