#!/bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
# input dataset at rucio
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta22_23_100000Evt_140520_FTFP_BERT_21-0-108-Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta22_23_100000Evt_140520_FTFP_BERT_G4-10-6.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta22_23_100000Evt_140520_FTFP_BERT_ATL_G4-10-6.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta22_23_100000Evt_140520_FTFP_BERT_ATL_21-0-108-Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_03_100000Evt_140520_FTFP_BERT_ATL_21-0-108-Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_03_100000Evt_140520_FTFP_BERT_ATL_G4-10-6.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_03_100000Evt_140520_FTFP_BERT_G4-10-6.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_03_100000Evt_140520_FTFP_BERT_21-0-108-Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta22_23_100000Evt_140520_FTFP_BERT_21-0-108-Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta22_23_100000Evt_140520_FTFP_BERT_G4-10-6.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta22_23_100000Evt_140520_FTFP_BERT_ATL_G4-10-6.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta22_23_100000Evt_140520_FTFP_BERT_ATL_21-0-108-Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_03_100000Evt_140520_FTFP_BERT_ATL_21-0-108-Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_03_100000Evt_140520_FTFP_BERT_ATL_G4-10-6.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_03_100000Evt_140520_FTFP_BERT_G4-10-6.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_03_100000Evt_140520_FTFP_BERT_21-0-108-Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_pt10GeV_10000Evt_170620_FTFP_BERT_G4-10-6.HITS_onlyAthSim_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_062420_FTFP_BERT_ATL_G4-10-6-LocalAthSim.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_062420_FTFP_BERT_G4-10-6-LocalAthSim.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_062420_FTFP_BERT_G4-10-1_21-0-108Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_062420_FTFP_BERT_ATL_G4-10-1_21-0-108Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_ATL_G4-10-1_21-0-108Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_G4-10-1_21-0-108Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_G4-10-6-LocalAthSim.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_ATL_G4-10-6-LocalAthSim.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_ATL_G4-10-6-21-0AthBLT.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_G4-10-6-21-0AthBLT.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_ATL_G4-10-1_BLOff.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_G4-10-1_BLOff.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_ATL_G4-10-6_AthSim.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_G4-10-6_AthSim.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_G4-10-1_108Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_ATL_G4-10-1_108Ath.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_ATL_G4-10-1_BLOff_Til.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_G4-10-1_BLOff_Til.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_G4-10-1_BLOff_Til.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_ATL_G4-10-1_BLOff_Til.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_ATL_G4-10-1_BLOff_LAr.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_G4-10-1_BLOff_LAr.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_G4-10-1_BLOff_LAr.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_ATL_G4-10-1_BLOff_LAr.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_FTFP_BERT_ATL_G4-10-1_BLOff_Til.HITS_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_G4-10-1_BLOff.HITS_EXT0"
INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_FTFP_BERT_ATL_G4-10-1_BLOff.HITS_EXT0"


########################################################################
#OUTPUT_DATASET_VOL
OUTPUT_DATASET_VOL=""

########################################################################
# number of jobs
#     - use int not string
N_FILES=100

########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################

########################################################################
# Automatic description and name of OUTPUT_DATASET
OUTPUT_DATASET_DESC_TMP_0=${INPUT_DATASET/HITS_EXT0/}
OUTPUT_DATASET_DESC=${OUTPUT_DATASET_DESC_TMP_0}${OUTPUT_DATASET_VOL}"AOD"
read -p "OUTPUT_DATASET_DESC: "${OUTPUT_DATASET_DESC}

########################################################################
# OUTPUT DATASET
#     do not modify
OUTPUT_DATASET=${OUTPUT_DATASET_DESC}
read -p "OUTPUT_DATASET: "${OUTPUT_DATASET}

########################################################################
# paths inicialization
BASE_DIR=`pwd`

WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${OUTPUT_DATASET_DESC}

mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}


########################################################################
# clear Working directory before submit
echo -e "\n\n"
echo "working directory have to be clean before grid submition"
echo "do you want to really remove all files/directories in:"
echo ${WORK_DIR_I}"/"
echo "[Y/N]"

while read CLEAR; do
   if [[ "${CLEAR}" == "Y" ]]; then
        echo -e "\n\n"
        ################################################################
        # Clear working directory
        rm -r ${WORK_DIR_I}/*
        
        ################################################################
        # go to working directory
        cd ${WORK_DIR_I}
        
        ################################################################
        # setup
        setupATLAS
        
        asetup 21.0.108,Athena,gcc62
        
        voms-proxy-init -voms atlas
        lsetup panda
        
        ################################################################
        # RUN with pathena
        echo -e "\n\n"
        echo "RUN with pathena"
        echo -e "\n\n"
        
        pathena \
--trf=" \
Reco_tf.py \
--AMIConfig r11627 \
--steering 'no' \
--preExec 'h2r:from Digitization.DigitizationFlags import jobproperties; jobproperties.Digitization.doCaloNoise=False' 'e2a:rec.Commissioning.set_Value_and_Lock(True)' 'r2e:from RecExConfig.RecFlags import rec; rec.doTrigger=False; rec.Commissioning.set_Value_and_Lock(True);' \
--DataRunNumber '284500' \
--maxEvents -1 \
--inputHITSFile %IN \
--outputAODFile %OUT.AOD.root \
--outputESDFile %OUT.ESD.root \
" \
--inDS  ${INPUT_DATASET} \
--outDS ${OUTPUT_DATASET} \
--nFiles ${N_FILES}

        cd ${BASE_DIR}
        break
    elif [[ "${CLEAR}" == "N" ]]; then
        echo "end of program"
        cd ${BASE_DIR}
        break
    else
        echo "wrong value!"
        echo "use: \"Y\" as YES"
        echo "     \"N\" as NO"
    fi
done
