#!/bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
# input dataset at rucio

# 0.2 < eta < 0.7
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_Barrel_100kEvt_120720_FTFP_BERT_ATL-G4-10-1.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-1.HIST_EXT0"
INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-1.HIST_EXT0"
# 2.15 < eta < 2.25
# for HEC
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_215_225_HEC_100kEvt_120720_FTFP_BERT_ATL-G4-10-1.HIST_EXT0"

# 3.8 < eta < 3.9
# for FCal
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_38_39_FCal_100kEvt_120720_FTFP_BERT_ATL-G4-10-1.HIST_EXT0"


########################################################################
# input dataset at rucio
# FTFP_BERT physics list
#INPUT_DATASET=""

SF_DESC=""
########################################################################
#OUTPUT_DATASET_VOL
OUTPUT_DATASET_VOL="_v2"

########################################################################
# number of jobs
#     - use int not string
#N_FILES=100

########################################################################
# number of events per one job
#     - use int not string
N_EVENTS_PER_JOB=5000

########################################################################
# number of jobs
#     - use int not string
N_JOBS=1000

########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################

########################################################################
# Automatic description and name of OUTPUT_DATASET
OUTPUT_DATASET_DESC_TMP_0=${INPUT_DATASET/.HIST_EXT0/}
OUTPUT_DATASET_DESC_TMP_1=${OUTPUT_DATASET_DESC_TMP_0/.HITS_EXT0/}
OUTPUT_DATASET_DESC_TMP_2=${OUTPUT_DATASET_DESC_TMP_1/.HIST_EXT0/}
OUTPUT_DATASET_DESC_TMP_3=${OUTPUT_DATASET_DESC_TMP_2/singleParticleSample/}
OUTPUT_DATASET_DESC_TMP_4=${OUTPUT_DATASET_DESC_TMP_3/_test/}
OUTPUT_DATASET_DESC_TMP_5=$OUTPUT_DATASET_DESC_TMP_4
if [[ "$OUTPUT_DATASET_DESC_TMP_5" == *"$FTFP_BERT_ATL"* ]]; then
  OUTPUT_DATASET_DESC_TMP_5=${OUTPUT_DATASET_DESC_TMP_5/FTFP_BERT_/}
elif [[ "$OUTPUT_DATASET_DESC_TMP_5" == *"$FTFP_BERT"* ]]; then
  OUTPUT_DATASET_DESC_TMP_5=${OUTPUT_DATASET_DESC_TMP_5/FTFP_/}
fi

OUTPUT_DATASET_DESC=${OUTPUT_DATASET_DESC_TMP_5}${SF_DESC}${OUTPUT_DATASET_VOL}".AOD"
read -p "OUTPUT_DATASET_DESC: "${OUTPUT_DATASET_DESC}


########################################################################
# OUTPUT DATASET
#     do not modify
OUTPUT_DATASET=${OUTPUT_DATASET_DESC}
read -p "OUTPUT_DATASET: "${OUTPUT_DATASET}

########################################################################
# paths inicialization
BASE_DIR=`pwd`

WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${OUTPUT_DATASET_DESC}

mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}


########################################################################
# clear Working directory before submit
echo -e "\n\n"
echo "working directory have to be clean before grid submition"
echo "do you want to really remove all files/directories in:"
echo ${WORK_DIR_I}"/"
echo "[Y/N]"

while read CLEAR; do
   if [[ "${CLEAR}" == "Y" ]]; then
        echo -e "\n\n"
        ################################################################
        # Clear working directory
        rm -r ${WORK_DIR_I}/*
        
        ################################################################
        # go to working directory
        cd ${WORK_DIR_I}
        
        ################################################################
        # setup
        setupATLAS
        
        asetup Athena,21.0.119,slc6
        
        voms-proxy-init -voms atlas
        lsetup panda
        
        ################################################################
        # RUN with pathena
        echo -e "\n\n"
        echo "RUN with pathena"
        echo -e "\n\n"
        
        pathena \
--trf=" \
Reco_tf.py \
--AMIConfig r11627 \
--steering 'no' \
--preExec 'h2r:from Digitization.DigitizationFlags import jobproperties; jobproperties.Digitization.doCaloNoise=False' 'e2a:rec.Commissioning.set_Value_and_Lock(True)' 'r2e:from RecExConfig.RecFlags import rec; rec.doTrigger=False; rec.Commissioning.set_Value_and_Lock(True);' \
--DataRunNumber '284500' \
--maxEvents -1 \
--inputHITSFile %IN \
--outputAODFile %OUT.AOD.root \
--outputESDFile %OUT.ESD.root \
" \
--inDS  ${INPUT_DATASET} \
--outDS ${OUTPUT_DATASET} \
--nEventsPerJob=${N_EVENTS_PER_JOB} \
--nJobs=${N_JOBS}

#--nFiles ${N_FILES}

        cd ${BASE_DIR}
        break
    elif [[ "${CLEAR}" == "N" ]]; then
        echo "end of program"
        cd ${BASE_DIR}
        break
    else
        echo "wrong value!"
        echo "use: \"Y\" as YES"
        echo "     \"N\" as NO"
    fi
done
