#!/bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
# input dataset at rucio
# test - grid
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_1kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1o_b2o_test.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_1kEvt_FTFP_BERT_ATL-G4-10-6-BLT__test.HIST_EXT0"
#


########################################################################
# input dataset at rucio
# FTFP_BERT_ATL physic list
# 100k samples
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1o_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1d_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1d_b2d.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1d_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1o_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1u_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1u_b2u.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1u_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1uu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1uuu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1dd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1ddd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1z_b2z.HIST_EXT0"
# 5000k samples
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1ddd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1dd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1d_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1uuu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1uu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1u_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1o_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1z_b2z.HIST_EXT0"
# 5M samples absEta
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1ddd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1dd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1d_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1o_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1u_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1uu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1uuu_b2z.HIST_EXT0"
INPUT_DATASET="user.ozaplati.13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_FTFP_BERT_ATL-G4-10-6-BLV_Tile_b1z_b2z.HIST_EXT0"


########################################################################
# input dataset at rucio
# FTFP_BERT physics list
# 100k samples
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1o_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1d_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1d_b2d.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1d_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1o_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1u_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1u_b2u.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1u_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1uu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1uuu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1dd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1ddd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1z_b2z.HIST_EXT0"


########################################################################
#OUTPUT_DATASET_VOL
OUTPUT_DATASET_VOL=""

########################################################################
# number of jobs
#     - use int not string
#N_FILES=100

########################################################################
# number of events per one job
#     - use int not string
N_EVENTS_PER_JOB=5000

########################################################################
# number of jobs
#     - use int not string
N_JOBS=1000

########################################################################
# substring - describe Sampling Fraction
#    _SFAll  .... with --postExec
#                       'all:tileInfoConfigurator.EmScaleA=32.9' 'all:conddb.addOverride(\"/LAR/ElecCalibMC/fSampl\",\"LARElecCalibMCfSampl-G4106-v1-FTFP_BERT_BIRK\")' \
#    _SFTile .... no postExec .... SF in Tile, as they are part of using latest nightly
SF_DESC="_SFAll"

########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
###############################################s#########################
########################################################################

########################################################################
# Automatic description and name of OUTPUT_DATASET
OUTPUT_DATASET_DESC_TMP_0=${INPUT_DATASET/.HIST_EXT0/}
OUTPUT_DATASET_DESC_TMP_1=${OUTPUT_DATASET_DESC_TMP_0/.HITS_EXT0/}
OUTPUT_DATASET_DESC_TMP_2=${OUTPUT_DATASET_DESC_TMP_1/.HIST_EXT0/}
OUTPUT_DATASET_DESC_TMP_3=${OUTPUT_DATASET_DESC_TMP_2/singleParticleSample/}
OUTPUT_DATASET_DESC_TMP_4=${OUTPUT_DATASET_DESC_TMP_3/_test/}
OUTPUT_DATASET_DESC_TMP_5=$OUTPUT_DATASET_DESC_TMP_4
if [[ "$OUTPUT_DATASET_DESC_TMP_5" == *"$FTFP_BERT_ATL"* ]]; then
  OUTPUT_DATASET_DESC_TMP_5=${OUTPUT_DATASET_DESC_TMP_5/FTFP_BERT_/}
elif [[ "$OUTPUT_DATASET_DESC_TMP_5" == *"$FTFP_BERT"* ]]; then
  OUTPUT_DATASET_DESC_TMP_5=${OUTPUT_DATASET_DESC_TMP_5/FTFP_/}
fi

OUTPUT_DATASET_DESC=${OUTPUT_DATASET_DESC_TMP_5}${SF_DESC}${OUTPUT_DATASET_VOL}".AOD"
read -p "OUTPUT_DATASET_DESC: "${OUTPUT_DATASET_DESC}

########################################################################
# OUTPUT DATASET
#     do not modify
OUTPUT_DATASET=${OUTPUT_DATASET_DESC}
read -p "OUTPUT_DATASET: "${OUTPUT_DATASET}

########################################################################
# paths inicialization
BASE_DIR=`pwd`

WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${OUTPUT_DATASET_DESC}

mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}


########################################################################
# clear Working directory before submit
echo -e "\n\n"
echo "working directory have to be clean before grid submition"
echo "do you want to really remove all files/directories in:"
echo ${WORK_DIR_I}"/"
echo "[Y/N]"

while read CLEAR; do
   if [[ "${CLEAR}" == "Y" ]]; then
        echo -e "\n\n"
        ################################################################
        # Clear working directory
        rm -r ${WORK_DIR_I}/*
        
        ################################################################
        # go to working directory
        cd ${WORK_DIR_I}
        
        ################################################################
        # setup
        setupATLAS
        
        #asetup Athena,21.0,slc6,latest     # used in November 2020 - 100k samples
        asetup Athena,21.0.119,slc6         # used in January 2021 - 5000k samples

        #voms-proxy-init -voms atlas
        lsetup panda
        
        ################################################################
        # RUN with pathena
        echo -e "\n\n"
        echo "RUN with pathena"
        echo -e "\n\n"
        
        pathena \
--trf=" \
Reco_tf.py \
--AMIConfig r11627 \
--steering 'no' \
--preExec 'h2r:from Digitization.DigitizationFlags import jobproperties; jobproperties.Digitization.doCaloNoise=False' 'e2a:rec.Commissioning.set_Value_and_Lock(True)' 'r2e:from RecExConfig.RecFlags import rec; rec.doTrigger=False; rec.Commissioning.set_Value_and_Lock(True);' \
--postExec 'all:tileInfoConfigurator.EmScaleA=32.9' 'all:conddb.addOverride(\"/LAR/ElecCalibMC/fSampl\",\"LARElecCalibMCfSampl-G4106-v1-FTFP_BERT_BIRK\")' \
--DataRunNumber '284500' \
--maxEvents -1 \
--inputHITSFile %IN \
--outputAODFile %OUT.AOD.root \
--outputESDFile %OUT.ESD.root \
" \
--inDS  ${INPUT_DATASET} \
--outDS ${OUTPUT_DATASET} \
--nEventsPerJob=${N_EVENTS_PER_JOB} \
--nJobs=${N_JOBS}

#--nFiles ${N_FILES}

        cd ${BASE_DIR}
        break
    elif [[ "${CLEAR}" == "N" ]]; then
        echo "end of program"
        cd ${BASE_DIR}
        break
    else
        echo "wrong value!"
        echo "use: \"Y\" as YES"
        echo "     \"N\" as NO"
    fi
done
