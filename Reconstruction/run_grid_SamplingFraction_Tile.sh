#!/bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
# input dataset at rucio
# test grid
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_1kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1o_b2o_test.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_1kEvt_FTFP_BERT_ATL-G4-10-6-BLT__test.HIST_EXT0"

########################################################################
# input dataset at rucio
# FTFP_BERT_ATL physics list
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1o_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1d_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1d_b2d.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1d_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1o_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1u_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1u_b2u.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1u_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1uu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1uuu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1dd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1ddd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT_ATL-G4-10-6-BLT_b1z_b2z.HIST_EXT0"


########################################################################
# input dataset at rucio
# FTFP_BERT physics list
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1o_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1d_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1d_b2d.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1d_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1o_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1u_b2o.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1u_b2u.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1u_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1uu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1uuu_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1dd_b2z.HIST_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1ddd_b2z.HIST_EXT0"
INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_FTFP_BERT-G4-10-6-BLT_b1z_b2z.HIST_EXT0"



########################################################################
#OUTPUT_DATASET_VOL
OUTPUT_DATASET_VOL=""

########################################################################
# number of jobs
#     - use int not string
N_FILES=100

########################################################################
# substring - describe Sampling Fraction
#    _SFAll  .... with --postExec
#                       'all:tileInfoConfigurator.EmScaleA=32.9' 'all:conddb.addOverride(\"/LAR/ElecCalibMC/fSampl\",\"LARElecCalibMCfSampl-G4106-v1-FTFP_BERT_BIRK\")' \
#    _SFTile .... no postExec .... SF in Tile, as they are part of using latest nightly
SF_DESC="_SFTil"

########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
###############################################s#########################
########################################################################

########################################################################
# Automatic description and name of OUTPUT_DATASET
OUTPUT_DATASET_DESC_TMP_0=${INPUT_DATASET/.HIST_EXT0/}
OUTPUT_DATASET_DESC_TMP_1=${OUTPUT_DATASET_DESC_TMP_0/_test/}
OUTPUT_DATASET_DESC=${OUTPUT_DATASET_DESC_TMP_1}${SF_DESC}${OUTPUT_DATASET_VOL}".AOD"
read -p "OUTPUT_DATASET_DESC: "${OUTPUT_DATASET_DESC}

########################################################################
# OUTPUT DATASET
#     do not modify
OUTPUT_DATASET=${OUTPUT_DATASET_DESC}
read -p "OUTPUT_DATASET: "${OUTPUT_DATASET}

########################################################################
# paths inicialization
BASE_DIR=`pwd`

WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${OUTPUT_DATASET_DESC}

mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}


########################################################################
# clear Working directory before submit
echo -e "\n\n"
echo "working directory have to be clean before grid submition"
echo "do you want to really remove all files/directories in:"
echo ${WORK_DIR_I}"/"
echo "[Y/N]"

while read CLEAR; do
   if [[ "${CLEAR}" == "Y" ]]; then
        echo -e "\n\n"
        ################################################################
        # Clear working directory
        rm -r ${WORK_DIR_I}/*
        
        ################################################################
        # go to working directory
        cd ${WORK_DIR_I}
        
        ################################################################
        # setup
        setupATLAS
        
        asetup Athena,21.0,slc6,latest
        
        #voms-proxy-init -voms atlas
        lsetup panda
        
        ################################################################
        # RUN with pathena
        echo -e "\n\n"
        echo "RUN with pathena"
        echo -e "\n\n"
        
        pathena \
--trf=" \
Reco_tf.py \
--AMIConfig r11627 \
--steering 'no' \
--preExec 'h2r:from Digitization.DigitizationFlags import jobproperties; jobproperties.Digitization.doCaloNoise=False' 'e2a:rec.Commissioning.set_Value_and_Lock(True)' 'r2e:from RecExConfig.RecFlags import rec; rec.doTrigger=False; rec.Commissioning.set_Value_and_Lock(True);' \
--DataRunNumber '284500' \
--maxEvents -1 \
--inputHITSFile %IN \
--outputAODFile %OUT.AOD.root \
--outputESDFile %OUT.ESD.root \
" \
--inDS  ${INPUT_DATASET} \
--outDS ${OUTPUT_DATASET} \
--nFiles ${N_FILES}

        cd ${BASE_DIR}
        break
    elif [[ "${CLEAR}" == "N" ]]; then
        echo "end of program"
        cd ${BASE_DIR}
        break
    else
        echo "wrong value!"
        echo "use: \"Y\" as YES"
        echo "     \"N\" as NO"
    fi
done
