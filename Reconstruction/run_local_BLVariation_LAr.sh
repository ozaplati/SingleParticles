#!bin/bash

setupATLAS
asetup Athena,21.0.119,slc6

#asetup 21.0.108,Athena,gcc62

#Reco_tf.py \
--AMIConfig r11627 \
--steering 'no' \
--preExec 'h2r:from Digitization.DigitizationFlags import jobproperties; jobproperties.Digitization.doCaloNoise=False' 'e2a:rec.Commissioning.set_Value_and_Lock(True)' 'r2e:from RecExConfig.RecFlags import rec; rec.doTrigger=False; rec.Commissioning.set_Value_and_Lock(True);' \
--postExec 'HITtoRDO:tileInfoConfigurator.EmScaleA=32.9' \
--postExec 'all:conddb.addOverride("/LAR/ElecCalibMC/fSampl","LARElecCalibMCfSampl-G4106-EMonly-FTFP_BERT_BIRK")' \
--DataRunNumber '284500' \
--maxEvents -1 \
--inputHITSFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt.HITS.root \
--outputAODFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt_SF_v3.AOD.root \
--outputESDFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt_SF_v3.ESD.root 

#Reco_tf.py \
--AMIConfig r11627 \
--steering 'no' \
--preExec 'h2r:from Digitization.DigitizationFlags import jobproperties; jobproperties.Digitization.doCaloNoise=False' 'e2a:rec.Commissioning.set_Value_and_Lock(True)' 'r2e:from RecExConfig.RecFlags import rec; rec.doTrigger=False; rec.Commissioning.set_Value_and_Lock(True);' \
--postExec 'HITtoRDO:tileInfoConfigurator.EmScaleA=32.9' 'all:conddb.addOverride("/LAR/ElecCalibMC/fSampl","LARElecCalibMCfSampl-G4106-EMonly-FTFP_BERT_BIRK")' \
--DataRunNumber '284500' \
--maxEvents -1 \
--inputHITSFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt.HITS.root \
--outputAODFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt_SF_v3_mySyntax.AOD.root \
--outputESDFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt_SF_v3_mySyntax.ESD.root 


#setupATLAS
#asetup 
Reco_tf.py \
--AMIConfig r11627 \
--steering 'no' \
--preExec  'h2r:from Digitization.DigitizationFlags import jobproperties; jobproperties.Digitization.doCaloNoise=False' 'e2a:rec.Commissioning.set_Value_and_Lock(True)' 'r2e:from RecExConfig.RecFlags import rec; rec.doTrigger=False; rec.Commissioning.set_Value_and_Lock(True);' \
--postExec 'all:tileInfoConfigurator.EmScaleA=32.9'  'all:conddb.addOverride("/LAR/ElecCalibMC/fSampl","LARElecCalibMCfSampl-G4106-EMonly-FTFP_BERT_BIRK")' \
--DataRunNumber '284500' \
--maxEvents -1 \
--inputHITSFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt.HITS.root \
--outputAODFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt_SF_v4_test_buildForBLVatiationLAr.AOD.root \
--outputESDFile /eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_eta_02_03_localtest_1Evt_SF_v4_test_buildForBLVatiationLAr.ESD.root 
