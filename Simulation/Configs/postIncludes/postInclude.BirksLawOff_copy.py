## postInclude.BirksLawOff.py
## LAr
ServiceMgr.EMBPresamplerCalculator.BirksLaw=False
ServiceMgr.EMBCalculator.BirksLaw=False
ServiceMgr.EMECPosInnerWheelCalculator.BirksLaw=False
ServiceMgr.EMECNegInnerWheelCalculator.BirksLaw=False
ServiceMgr.EMECPosOuterWheelCalculator.BirksLaw=False
ServiceMgr.EMECNegOuterWheelCalculator.BirksLaw=False
ServiceMgr.EMECPresamplerCalculator.BirksLaw=False
### EMEC BOB
ServiceMgr.EMECBackOuterBarretteCalculator.BirksLaw=False ## 21.0
## ServiceMgr.EMECPosBackOuterBarretteCalculator.BirksLaw=False ## master
## ServiceMgr.EMECNegBackOuterBarretteCalculator.BirksLaw=False ## master
###
ServiceMgr.FCAL1Calculator.BirksLaw=False
ServiceMgr.FCAL2Calculator.BirksLaw=False
ServiceMgr.FCAL3Calculator.BirksLaw=False
ServiceMgr.HECWheelCalculator.BirksLaw=False

#Tile
ServiceMgr.TileGeoG4SDCalc.DoBirk=False
#MBTS - has no effect - ATLASSIM-4652
ToolSvc.SensitiveDetectorMasterTool.SensitiveDetectors["MinBiasScintillatorSD"].DoBirk=False
