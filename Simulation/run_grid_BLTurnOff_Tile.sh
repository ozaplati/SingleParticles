#!/bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
# input dataset at rucio
INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.electron_eta02_07_logP_100kEvt_071420_EXT0"

########################################################################
# output dataset at rucio
#OUTPUT_DATASER="user.ozaplati.singleParticleSample13TeV.MC15.999999.ParticleGun_single_piplus_eta_02_03_2000Evt_vol6.HIST"

########################################################################
# physics list
#    e.q.
#         PHYSICS_LIST='FTFP_BERT'
#         PHYSICS_LIST='FTFP_BERT_ATL'
PHYSICS_LIST='FTFP_BERT_ATL'  


########################################################################
# number of events per one job
#     - use int not string
N_EVENTS_PER_JOB=1000

########################################################################
# number of jobs
#     - use int not string
N_JOBS=100

########################################################################
# total number of events = N_JOBS * N_EVENTS_PER_JOB
#     - do not change
N_EVENTS_TOT="$(( ${N_JOBS}*${N_EVENTS_PER_JOB} ))"

POSTINCLUDE_FILE="postInclude.BirksLawOff_Tile.py"

########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################
BASE_DIR=${PWD}
########################################################################
# Automatic description and name of OUTPUT_DATASET

########################################################################
# description
#     do not modify
#     - OUTPUT_DATASET_DESC ... for a name of output container

ADD_DESC="_"${PHYSICS_LIST}"_G4-10-1_BLOff_Til.HITS"

OUPUT_DATASET_DESC_TMP=${INPUT_DATASET/_EXT0/}
OUPUT_DATASET_DESC=${OUPUT_DATASET_DESC_TMP##*.}${ADD_DESC}
echo $OUPUT_DATASET_DESC

read -p "wait"
########################################################################
# OUTPUT DATASET
#     do not modify
OUTPUT_DATASET_TMP=${INPUT_DATASET/_EXT0/}
OUTPUT_DATASET_TMP1=${OUTPUT_DATASET_TMP/EoverP_/}
OUTPUT_DATASET=${OUTPUT_DATASET_TMP1/ParticleGun_/}${ADD_DESC}
echo $OUTPUT_DATASET

read -p "wait"



########################################################################
# paths inicialization
BASE_DIR=`pwd`

WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${OUPUT_DATASET_DESC}

mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}


########################################################################
# clear Working directory before submit
echo -e "\n\n"
echo "working directory have to be clean before grid submition"
echo "do you want to really remove all files/directories in:"
echo ${WORK_DIR_I}"/"
echo "[Y/N]"

while read CLEAR; do
   if [[ "${CLEAR}" == "Y" ]]; then
        echo -e "\n\n"
        ################################################################
        # Clear working directory
        rm -r ${WORK_DIR_I}/*
        
        ################################################################
        # go to working directory
        cd ${WORK_DIR_I}
        
        ################################################################
        # setup
        setupATLAS
        
        asetup 21.0,Athena,gcc62,slc6,r2020-07-03T2145
        
        voms-proxy-init -voms atlas
        lsetup panda
        
        
        ################################################################
        # copy postInclude file
        cp -v ${BASE_DIR}/PostIncludes/${POSTINCLUDE_FILE} . 
        
        read -p "copy postInclude"
        
        ################################################################
        # RUN with pathena
        echo -e "\n\n"
        echo "RUN with pathena"
        echo -e "\n\n"
        
        pathena \
--trf=" \
Sim_tf.py \
--conditionsTag 'default:OFLCOND-MC16-SDR-14' \
--physicsList '${PHYSICS_LIST}' \
--truthStrategy 'MC15aPlus' \
--simulator 'FullG4' \
--postInclude 'EVNTtoHITS:./"${POSTINCLUDE_FILE}"' 'default:PyJobTransforms/UseFrontier.py' \
--preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py' \
--preExec 'EVNTtoHITS:simFlags.TightMuonStepping=True' \
--DataRunNumber '284500' \
--geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
--inputEVNTFile %IN \
--outputHITSFile %OUT.HITS.root \
--maxEvents -1 \
--imf False \
" \
--inDS  ${INPUT_DATASET} \
--outDS ${OUTPUT_DATASET} \
--nEventsPerJob=${N_EVENTS_PER_JOB} \
--nJobs=${N_JOBS}

        cd ${BASE_DIR}
        break
    elif [[ "${CLEAR}" == "N" ]]; then
        echo "end of program"
        cd ${BASE_DIR}
        break
    else
        echo "wrong value!"
        echo "use: \"Y\" as YES"
        echo "     \"N\" as NO"
    fi
done

