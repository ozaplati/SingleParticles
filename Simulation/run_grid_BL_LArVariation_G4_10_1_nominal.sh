#!/bin/bash
########################################################################
########################################################################
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
# input dataset at rucio
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_063020_EXT0"

#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.PG_piplus_eta_02_07_Barrel_100kEvt_120720_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.PG_piplus_eta_38_39_FCal_100kEvt_120720_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.PG_piplus_eta_215_225_HEC_100kEvt_120720_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_3000kEvt_011521_v2_EXT0"
#INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.PG_piplus_eta_02_07_lowP_Barrel_5000kEvt_011721_EXT0"
INPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.PG_piplus_absEta_02_07_lowP_Barrel_5MEvt_042121_EXT0"

########################################################################
# physics list
#    e.q.
#         PHYSICS_LIST='FTFP_BERT'
#         PHYSICS_LIST='FTFP_BERT_ATL'
PHYSICS_LIST='FTFP_BERT_ATL'  


########################################################################
# number of events per one job
#     - use int not string
N_EVENTS_PER_JOB=5000

########################################################################
# number of jobs
#     - use int not string
N_JOBS=1000
#N_JOBS=1

########################################################################
# total number of events = N_JOBS * N_EVENTS_PER_JOB
#     - do not change
N_EVENTS_TOT="$(( ${N_JOBS}*${N_EVENTS_PER_JOB} ))"



########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################
BASE_DIR=${PWD}
echo ""
echo ""
echo ${BASE_DIR}
echo ""
read -p "press a button to continue"

########################################################################
# Automatic description and name of OUTPUT_DATASET

########################################################################
# description
#     do not modify
#     - OUTPUT_DATASET_DESC ... for a name of output container

########################################################################
# output dataset at rucio

INPUT_DATASET_TMP=${INPUT_DATASET/EXT0/}
INPUT_DATASET_TMP2=${INPUT_DATASET_TMP/singleParticleSample/}
OUTPUT_DATASET=${INPUT_DATASET_TMP2}${PHYSICS_LIST}"-G4-10-1.HIST"
#
#OUTPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_100kEvt_"${PHYSICS_LIST}"-G4-10-6-BLT_"${BL_VARIATION_TAG}".HIST"
#OUTPUT_DATASET="user.ozaplati.singleParticleSample13TeV.MC15.999999.piplus_eta02_07_logP_1kEvt_"${PHYSICS_LIST}"-G4-10-6-BLT_"${BL_VARIATION_TAG}"_test.HIST"
OUTPUT_DATASET_DESC=${OUTPUT_DATASET/user.ozaplati.singleParticleSample13TeV.MC15.999999./}
echo "OUTPUT_DATASET_DESC: "${OUTPUT_DATASET_DESC}
echo "OUTPUT_DATASET: "${OUTPUT_DATASET}
echo ""
read -p "press a button to continue"



########################################################################
# paths inicialization
BASE_DIR=`pwd`

WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${OUTPUT_DATASET_DESC}

mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}


########################################################################
# clear Working directory before submit
echo -e "\n\n"
echo "working directory have to be clean before grid submition"
echo "do you want to really remove all files/directories in:"
echo ${WORK_DIR_I}"/"
echo "[Y/N]"

while read CLEAR; do
   if [[ "${CLEAR}" == "Y" ]]; then
		echo -e "\n\n"
		################################################################
		# Clear working directory
		rm -r ${WORK_DIR_I}/*
		
		################################################################
		# go to working directory
		cd ${WORK_DIR_I}
		
		
		################################################################
		# setup Athena - G4 10.1
		asetup 21.0.108,Athena,gcc62
		voms-proxy-init -voms atlas
		lsetup panda
		
		
		################################################################
		# RUN with pathena
		echo -e "\n\n"
		echo "RUN with pathena"
		echo -e "\n\n"
		
		read -p "press a button to continue"
		
		pathena \
--trf=" \
Sim_tf.py \
--conditionsTag 'default:OFLCOND-MC16-SDR-14' \
--physicsList '${PHYSICS_LIST}' \
--truthStrategy 'MC15aPlus' \
--simulator 'FullG4' \
--postInclude 'default:PyJobTransforms/UseFrontier.py' \
--preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py' \
--preExec 'EVNTtoHITS:simFlags.TightMuonStepping=True' \
--DataRunNumber '284500' \
--geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
--inputEVNTFile %IN \
--outputHITSFile %OUT.HITS.root \
--maxEvents -1 \
--imf False \
" \
--inDS  ${INPUT_DATASET} \
--outDS ${OUTPUT_DATASET} \
--nEventsPerJob=${N_EVENTS_PER_JOB} \
--nJobs=${N_JOBS}

		cd ${BASE_DIR}
		break
	elif [[ "${CLEAR}" == "N" ]]; then
		echo "end of program"
		cd ${BASE_DIR}
		break
	else
		echo "wrong value!"
		echo "use: \"Y\" as YES"
		echo "     \"N\" as NO"
	fi
done
