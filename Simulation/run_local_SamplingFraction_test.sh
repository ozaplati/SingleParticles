#!/bin/sh
#!/bin/bash
########################################################################
########################################################################
##                          SIMULATION                                ##
##                       Sim_tf.py <args>                             ##
##                                                                    ##
##                          SETUP PART                                ##
########################################################################
########################################################################

########################################################################
#  path and name of input EVNT file
#    e.q.
#        INPUT_EVNT="/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus//ENVT//single_piplus_eta_22_23.root"
INPUT_EVNT="/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/ENVT/user.ozaplati.singleParticleSample13TeV.MC15.999999.ParticleGun_piplus_pt10GeV_10000Evt_170620_EXT0/user.ozaplati.21636380.EXT0._000001.EVNT.pool.root"

########################################################################
# path and name of output HITS file
#    e.q.
#        OUTPUT_HITS="/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus//HITS/piplus_eta_02_03_localtest_1Evt_FTFP_BERT.HITS.root"
#        OUTPUT_HITS="/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus//HITS//single_piplus_eta_22_23_100Evt.HITS.root"
OUTPUT_HITS="/eos/home-o/ozaplati/Validation/SingleParticleSamples/piplus/HITS/piplus_pt10GeV_10000Evt_170620_3Evt_2020-20-08-SamplingFraction_test2_G4-10-6.HITS.root"

########################################################################
# physics list
#    e.q.
#         PHYSICS_LIST='FTFP_BERT'
#         PHYSICS_LIST='FTFP_BERT_ATL'
PHYSICS_LIST='FTFP_BERT'  

########################################################################
# number of events
#    e.q.
#        N_EVENTS="1"
N_EVENTS="3"

########################################################################
# If you want to test special verison of Athena/AthSimulation/...      #
#                      just modify the asetup                          #
########################################################################



########################################################################
########################################################################
##                     MAIN PART OF THE SCRIPT                        ##
########################################################################
########################################################################

########################################################################
# description
#     - for a Woking subDir name
FILE_BASENAME=`echo $(basename $INPUT_EVNT) | sed "s/\..*//"`
DESC="local_"${FILE_BASENAME}"_${N_EVENTS}Evt"

########################################################################
# paths inicialization
BASE_DIR=`pwd`
WORK_DIR=${BASE_DIR}"/Work"
WORK_DIR_I=${WORK_DIR}"/"${DESC}

########################################################################
# make Work directories
mkdir -p ${WORK_DIR}
mkdir -p ${WORK_DIR_I}

if [ ! -f ${INPUT_EVNT} ]; then
    ####################################################################
    # The input EVNT file: ${INPUT_EVNT} does not exist
    # ->exit 
    echo -e "\n\n"
    echo "ERROR"
    echo "input EVNT file:"
    echo "${INPUT_EVNT}"
    echo "does not exist!"

else
    ####################################################################
    # OK, jobOption exits
    # lets prepare working directory
    
    cd ${WORK_DIR_I}
    
    ####################################################################
    # Setup
    aklog
    setupATLAS
    
    # default asetup
    #asetup 21.0.108,Athena,gcc62
    
    # privite setup for G4 10.6
    asetup local/simulation/21.0,2020-03-23T1702,AthSimulation
    
    #asetup local/simulation/master,2020-06-08T1530,Athena
    #asetup local/simulation/21.0,2020-06-08T1530,Athena
    #asetup local/simulation/21.0,2020-06-08T1530,Athena
    
    #############################
    # execute command
    Sim_tf.py \
--conditionsTag 'default:OFLCOND-MC16-SDR-14' \
--physicsList ${PHYSICS_LIST} \
--truthStrategy 'MC15aPlus' \
--simulator 'FullG4' \
--postInclude 'default:PyJobTransforms/UseFrontier.py' \
--preInclude 'EVNTtoHITS:SimulationJobOptions/preInclude.BeamPipeKill.py' \
--preExec 'EVNTtoHITS:simFlags.TightMuonStepping=True' \
--postExec 'all:conddb.addOverride("/LAR/ElecCalibMC/fSampl","LARElecCalibMCfSampl-G4106-EMonly-FTFP_BERT_BIRK")' \
--DataRunNumber '284500' \
--geometryVersion 'default:ATLAS-R2-2016-01-00-01' \
--inputEVNTFile ${INPUT_EVNT} \
--outputHITSFile ${OUTPUT_HITS} \
--maxEvents ${N_EVENTS} \
--imf False

    cd ${BASE_DIR}
fi
